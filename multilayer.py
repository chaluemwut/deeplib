import tensorflow as tf

class MultilayerNeuralNetwork():
    learning_rate = 0.01
    num_episode = 1000
    
    def train(self, x, y):
        x_len = len(x[0])
        y_len = len(y[0])
        
        self.x_var = tf.placeholder(tf.float32, [None, x_len])
        W = tf.Variable(tf.zeros([x_len, y_len]))
        b = tf.Variable(tf.zeros([y_len]))
        self.y_pred = tf.nn.softmax(tf.matmul(self.x_var, W) + b)
        y_true = tf.placeholder(tf.float32, [None, y_len])
        
        cross_ent = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.y_pred, y_true))
        train_step = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(cross_ent)
        
        self.sess = tf.Session()
        self.sess.run(tf.initialize_all_variables())
        for _ in range(self.num_episode):
            self.sess.run(train_step, feed_dict={self.x_var: x, y_true: y})
            
    def predict(self, x):
        y_output = tf.argmax(self.y_pred, 1)
        return self.sess.run(y_output, feed_dict={self.x_var: x})
    
if __name__ == '__main__':
    from tensorflow.examples.tutorials.mnist import input_data
    mnist = input_data.read_data_sets('/tmp/data/', one_hot=True)
    x_train, y_train = mnist.train.next_batch(500)
    x_test, y_test = mnist.test.next_batch(200)
    
    multi_nn = MultilayerNeuralNetwork()
    multi_nn.train(x_train, y_train)
    print(multi_nn.predict(x_test))
    